"""[summary]

Returns:
    [type]: [description]
"""


class Board:
    """[summary]

    Returns:
        [type]: [description]
    """

    def __init__(self):
        self.length = 9
        self.board = [[0 for j in range(self.length)] for i in range(self.length)]

    def __str__(self):
        str_ = ''
        for i in range(self.length):
            if i == 0:
                str_ = '      1   2   3   4   5   6   7   8   9\n'
                str_ += '    +~~~~~~~~~~~+~~~~~~~~~~~+~~~~~~~~~~~+\n'
            elif i % 3 == 0:
                str_ += '    +~~~~~~~~~~~+~~~~~~~~~~~+~~~~~~~~~~~+\n'
            else:
                str_ += '    +-----------------------------------+\n'

            for j in range(self.length):
                if j == 0:
                    str_ += '  ' + str(i + 1) + ' | ' + \
                        str(self.board[i][j]) + ' '
                else:
                    str_ += '| ' + str(self.board[i][j]) + ' '
            str_ += '|\n'

        str_ += '    +~~~~~~~~~~~+~~~~~~~~~~~+~~~~~~~~~~~+\n'

        return str_

    def insert_values(self):
        """[summary]
        """

        counter = 0
        while counter != self.length:
            row = counter
            counter += 1
            line = input("  Row " + str(counter) + ": ")
            if line:
                col = 0
                for value in list(line):
                    try:
                        temp = int(value)
                    except ValueError:
                        print("Input only expects integer (0-9) values, try again.")
                        while True:
                            cell_value = input(
                                "  Cell({0},{1}) : ".format(row+1, col+1))
                            if not cell_value.isdecimal():
                                print("Wrong again!")
                                continue
                            temp = int(cell_value)
                            break

                    if temp:
                        self.board[row][col] = temp
                    col += 1
